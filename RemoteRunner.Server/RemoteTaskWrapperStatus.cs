﻿namespace RemoteRunner.Server
{
    internal class RemoteTaskWrapperStatus
    {
        internal const int NotStarted = 0;
        internal const int Running = 1;
        internal const int Completed = 2;
        internal const int Failed = 3;
    }
}
