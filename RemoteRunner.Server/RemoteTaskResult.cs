﻿namespace RemoteRunner.Server
{
    public sealed class RemoteTaskResult
    {
        public RemoteTask Task { get; private set; }
        public string Result { get; private set; }
        public bool Completed { get; private set; }

        public RemoteTaskResult(RemoteTask task, string result, bool completed)
        {
            Task = task;
            Result = result;
            Completed = completed;
        }
    }
}
