﻿using System;

namespace RemoteRunner.Server
{
    public sealed class RemoteTask
    {
        public string TaskId { get; private set; }

        public string TaskData { get; private set; }

        public RemoteTask(string taskData)
        {
            TaskId = Guid.NewGuid().ToString("N");
            TaskData = taskData ?? string.Empty;
        }
    }
}
