﻿using RemoteRunner.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading;

namespace RemoteRunner.Server
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    internal class RemoteTaskEngine : IRemoteTaskEngine
    {
        private readonly Uri baseAddress;
        private readonly IEnumerable<Uri> agentList;

        public RemoteTaskEngine(Uri baseAddress, IEnumerable<Uri> agentList)
        {
            this.baseAddress = baseAddress;
            this.agentList = agentList;
        }

        private List<RemoteTaskWrapper> tasks;
        private ManualResetEvent waitHandle;

        public void Run(IEnumerable<RemoteTaskWrapper> taskWrappers)
        {
            waitHandle = new ManualResetEvent(false);
            tasks = new List<RemoteTaskWrapper>(taskWrappers);

            // Create the ServiceHost.
            using (ServiceHost host = new ServiceHost(this, baseAddress))
            {
                // Open the ServiceHost to start listening for messages. Since
                // no endpoints are explicitly configured, the runtime will create
                // one endpoint per base address for each service contract implemented
                // by the service.
                host.Open();

                using (System.Timers.Timer timer = new System.Timers.Timer(1000))
                {
                    timer.AutoReset = true;
                    timer.Elapsed += (sender, e) =>
                    {
                        foreach (var task in tasks)
                            if (task.IsTimeout)
                                task.Reset();

                        if (!tasks.Any(t => t.Status != RemoteTaskWrapperStatus.Completed && t.Status != RemoteTaskWrapperStatus.Failed))
                            waitHandle.Set();
                    };
                    timer.Start();

                    NotifyAgents();

                    waitHandle.WaitOne();

                    timer.Stop();
                }

                host.Close();
            }
        }

        private void NotifyAgents()
        {
            try
            {
                BasicHttpBinding binding = new BasicHttpBinding();
                ChannelFactory<IRemoteTaskAgent> factory = new ChannelFactory<IRemoteTaskAgent>(binding);
                foreach (var agentAddress in agentList)
                {
                    try
                    {
                        var agent = factory.CreateChannel(new EndpointAddress(agentAddress));
                        agent.StartRun(baseAddress.AbsoluteUri);
                    }
                    catch (Exception e)
                    {
                    }
                }
                factory.Close();
            }
            catch (Exception e)
            {
            }
        }

        public IEnumerable<RemoteTaskWrapper> Tasks
        {
            get { return tasks; }
        }

        public RemoteTaskInfo StartNewTask()
        {
            var task = tasks.FirstOrDefault(t => t.Status == RemoteTaskWrapperStatus.NotStarted && t.BeginExecution());
            if (task != null)
                return new RemoteTaskInfo()
                {
                    TaskId = task.Task.TaskId,
                    TaskData = task.Task.TaskData,
                    RemoteTaskStatus = RemoteTaskStatus.TaskReady
                };

            if (tasks.Any(t => t.Status != RemoteTaskWrapperStatus.Completed && t.Status != RemoteTaskWrapperStatus.Failed))
                return new RemoteTaskInfo()
                {
                    RemoteTaskStatus = RemoteTaskStatus.AskAgainLater
                };

            return new RemoteTaskInfo()
            {
                RemoteTaskStatus = RemoteTaskStatus.NoMoreTasks
            };
        }

        public void CompleteTask(string taskId, string taskResult)
        {
            if (taskId == null || taskResult == null)
                return;

            var task = tasks.FirstOrDefault(t => t.Task.TaskId == taskId);
            if (task == null)
                return;

            task.CompleteExecution(taskResult);

            if (tasks.Any(t => t.Status != RemoteTaskWrapperStatus.Completed && t.Status != RemoteTaskWrapperStatus.Failed))
                return;
            waitHandle.Set();
        }
    }
}
