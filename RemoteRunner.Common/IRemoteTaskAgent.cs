﻿using System.ServiceModel;

namespace RemoteRunner.Common
{
    [ServiceContract]
    public interface IRemoteTaskAgent
    {
        [OperationContract]
        void StartRun(string serverAddress);
    }
}
