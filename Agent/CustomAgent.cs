﻿using RemoteRunner.Agent;
using RemoteRunner.Common;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace Agent
{
    class CustomAgent : RemoteAgent
    {
        protected override string RunTask(RemoteTaskInfo task)
        {
            Console.WriteLine("running task {0} with data {1}", task.TaskId, task.TaskData);

            string outputPath = Path.GetTempFileName();
            string outputDir = Path.GetDirectoryName(outputPath);
            string outputFileName = Path.GetFileName(outputPath);
            string basePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string dllPath = Path.Combine(basePath, "Nunit\\SampleTests.dll");

            ProcessStartInfo processStartInfo = new ProcessStartInfo();
            processStartInfo.Arguments = string.Format("{0} /include:{1} /work:\"{2}\" /xml:{3}", dllPath, task.TaskData, outputDir, outputPath);
            processStartInfo.FileName = Path.Combine(basePath, "Nunit\\nunit-console.exe");

            Process p = Process.Start(processStartInfo);
            p.WaitForExit((int)TimeSpan.FromMinutes(5).TotalMilliseconds);

            if (!p.HasExited)
                return null;

            string result = File.ReadAllText(outputPath);
            File.Delete(outputPath);

            if (string.IsNullOrEmpty(result))
                return null;

            return result;
        }
    }
}
