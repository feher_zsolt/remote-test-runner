﻿using RemoteRunner.Common;
using System;
using System.ServiceModel;
using System.Threading;

namespace RemoteRunner.Agent
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public abstract class RemoteAgent : IRemoteTaskAgent
    {
        private ServiceHost host;

        public void Open(Uri baseAddress)
        {
            host = new ServiceHost(this, baseAddress);

            // Open the ServiceHost to start listening for messages. Since
            // no endpoints are explicitly configured, the runtime will create
            // one endpoint per base address for each service contract implemented
            // by the service.
            host.Open();
        }

        public void Close()
        {
            if (host != null)
                host.Close();
        }

        public void StartRun(string serverAddress)
        {
            Thread newThread = new Thread(() =>
            {
                try
                {
                    BasicHttpBinding binding = new BasicHttpBinding();
                    using (ChannelFactory<IRemoteTaskEngine> channelFactory = new ChannelFactory<IRemoteTaskEngine>(binding))
                    {
                        IRemoteTaskEngine taskEngine = channelFactory.CreateChannel(new EndpointAddress(serverAddress));

                        while (true)
                        {
                            RemoteTaskInfo task = taskEngine.StartNewTask();
                            if (task == null)
                                break;

                            if (task.RemoteTaskStatus == RemoteTaskStatus.NoMoreTasks)
                                break;

                            if (task.RemoteTaskStatus == RemoteTaskStatus.AskAgainLater)
                            {
                                Thread.Sleep(1000);
                                continue;
                            }

                            string result = null;
                            try
                            {
                                result = RunTask(task);
                            }
                            catch
                            {
                                Console.WriteLine("Error during execution of task.");
                                taskEngine.CompleteTask(task.TaskId, null);
                                break;
                            }
                            if (result != null)
                                taskEngine.CompleteTask(task.TaskId, result);
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            });

            newThread.Start();
        }

        protected abstract string RunTask(RemoteTaskInfo task);
    }
}
